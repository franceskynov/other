//
//  utils.swift
//  Other
//
//  Created by Dev on 10/17/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class Utils {
    
    static func fieldLength(field: UITextField) -> Int {
        return field.text?.count ?? 0
    }
    
    static func isValidEmail(email: String) -> Bool {
        let regexpr = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let validateEmail = NSPredicate(format: "SELF MATCHES %@", regexpr)
        return validateEmail.evaluate(with: email)
    }
    
    static func validatePasswd(passwd: String, confirm: String) -> String {
        if (passwd == confirm) && (passwd.count >= 5 && passwd.count >= 5)  {
            
            return "new_passwd"
            
        } else if (passwd == confirm) && (passwd.count == 0 && passwd.count == 0)  {
            
            return "use_current_passwd"
            
        } else {
            
            return "invalid_passwd"
        }
    }

}
