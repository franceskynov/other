//
//  User.swift
//  Other
//
//  Created by Dev on 10/15/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class User: NSObject {
    var id: Int?
    var user: String?
    var username: String?
    var email: String?
    var country: String?
    var password: String?
    var age: Int?
    
    init(user: String, username: String, email: String, country: String, password: String, age: Int) {
        self.user = user
        self.username = username
        self.email = email
        self.country = country
        self.password = password
        self.age = age
    }
    
    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as? Int
        self.user = dictionary["name"] as? String
        self.email = dictionary["email"] as? String
        self.username = dictionary["username"] as? String
        self.country = dictionary["country"] as? String
        self.password = dictionary["password"] as? String
        self.age = dictionary["age"] as? Int
    }
}
