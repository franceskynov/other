//
//  UserService.swift
//  Other
//
//  Created by Dev on 10/15/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

class UserService {
    static let header = [
        "Content-Type": "application/json; charset=utf-8"
    ]
    
    static func getUsers(completion: @escaping ([User]) -> Void) {
        Alamofire.request(Constants.urlApi + "users", headers: header).responseJSON { (response) in
            var users = [User]()
            if response.result.error == nil {
                
                let objects = response.result.value as? [Dictionary <String, AnyObject>]
                for object in objects! {
                    users.append(User(dictionary: object))
                }
                
                completion(users)
                
            } else {
                
                completion(users)
            }
        }
    }
    
    static func registerUser(user: User, completation: @escaping (Bool?) -> Void) {
        
        let body: [String: Any] = [
            "name": user.user!,
            "username": user.username!,
            "email": user.email!,
            "country": user.country!,
            "password": user.password!,
            "age" : user.age!
        ]
        
        Alamofire.request(Constants.urlApi + "users", method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseString{ (response) in
            if response.result.error == nil {
                completation(true)
            } else {
                completation(false)
            }
        }
    }
    
    static func deleteUser(id: Int, completation: @escaping (Bool?) -> Void) {
        Alamofire.request(Constants.urlApi + "users/\(id)", method: .delete).responseJSON { (response) in
            if response.result.error == nil {
                completation(true)
            } else {
                completation(false)
            }
        }
    }
    
    static func updateUser(id: Int, user: User, completation: @escaping (Bool?) -> Void) {
        let body: [String: Any] = [
            "name": user.user!,
            "username": user.username!,
            "email": user.email!,
            "country": user.country!,
            "password": user.password!,
            "age" : user.age!
        ]
        
        Alamofire.request(Constants.urlApi + "users/\(id)", method: .patch, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.error == nil {
                completation(true)
            } else {
                completation(false)
            }
        }
    }

}
