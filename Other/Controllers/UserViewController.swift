//
//  UserViewController.swift
//  Other
//
//  Created by Dev on 10/15/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit
import Foundation

class UserViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var ageSlider: UISlider!
    @IBOutlet weak var ageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeAge(_ sender: UISlider) {
         self.ageLabel.text = String(format: "%d", Int(sender.value))
    }
    
    @IBAction func create(_ sender: UIButton) {
    
        
        if Utils.fieldLength(field: nameField) >= 3 &&
           Utils.fieldLength(field: usernameField) >= 3 &&
           Utils.fieldLength(field: emailField) >= 3 &&
           Utils.fieldLength(field: countryField) >= 3 &&
           Utils.fieldLength(field: passwordField) >= 5 &&
           Utils.isValidEmail(email: emailField.text!) {
            
            let u = User(user: nameField.text!,
                         username: usernameField.text!,
                         email: emailField.text!,
                         country: countryField.text!,
                         password: passwordField.text!,
                         age: Int(ageSlider.value))
            
            UserService.registerUser(user: u, completation: { (success) -> Void in
                var alert: UIAlertController
                
                if success! {
                    print("User created")
                    alert = UIAlertController(title: "Creacion de usuario", message: "El usuario se creo exitosamente", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        self.changeView()
                    }))
                    
                    self.present(alert, animated: true)
                    
                } else {
                    
                    print("User not as been created")
                    alert = UIAlertController(title: "Creacion de usuario", message: "Error en la creacion del usuario ", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
                
            })

        } else {
            var alert: UIAlertController
            alert = UIAlertController(title: "Error ", message: "No cumple con las reglas", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    
    func changeView() {
        performSegue(withIdentifier: "listUsers", sender: self)
        self.clearFrm()
    }
    
    func clearFrm() {
        self.nameField.text = ""
        self.usernameField.text = ""
        self.countryField.text = ""
        self.passwordField.text = ""
        self.emailField.text = ""
        self.ageSlider.value = 0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
