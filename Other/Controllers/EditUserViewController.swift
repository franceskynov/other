//
//  EditUserViewController.swift
//  Other
//
//  Created by Dev on 10/19/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class EditUserViewController: UIViewController {
    
    @IBOutlet weak var fullnameField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var currentPasswordField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var ageSlider: UISlider!
    
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let user = user {
            fullnameField.text = user.user
            usernameField.text = user.username
            emailField.text = user.email
            countryField.text = user.email
            ageSlider.value = Float(user.age ?? 0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func saveUserData(_ sender: UIBarButtonItem) {
        var alert: UIAlertController!
        var u: User?
        
        if currentPasswordField.text == user?.password {
            
            if Utils.fieldLength(field: fullnameField) >= 3 &&
                Utils.fieldLength(field: usernameField) >= 3 &&
                Utils.fieldLength(field: emailField) >= 3 &&
                Utils.fieldLength(field: countryField) >= 3 &&
                Utils.isValidEmail(email: emailField.text!) {
                
                let newPasswd = Utils.validatePasswd(passwd: passwordField.text!, confirm: confirmPasswordField.text!)
                var flag: Bool
                if newPasswd == "new_passwd" {
                    u =  User(user: fullnameField.text!,
                              username: usernameField.text!,
                              email: emailField.text!,
                              country: countryField.text!,
                              password: passwordField.text!,
                              age: Int(ageSlider.value))
                    flag = true
                } else if newPasswd == "use_current_passwd" {
                    
                    u = User(user: fullnameField.text!,
                             username: usernameField.text!,
                             email: emailField.text!,
                             country: countryField.text!,
                             password: currentPasswordField.text!,
                             age: Int(ageSlider.value))
                    flag = true
                } else {
                    u = user
                    alert = UIAlertController(title: "Error", message: "Los passwords no coinciden", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    flag = false
                }
                
                if flag == true {
                    UserService.updateUser(id: (user?.id!)!, user: u! , completation: { (success) -> Void in
                        var alertC: UIAlertController!
                        if success! {
                            
                            alertC = UIAlertController(title: "Modificacion", message: "El usuario fue modificado correctamente", preferredStyle: .alert)
                            alertC.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                self.toList()
                            }))
                            
                            
                        } else {
                            alertC = UIAlertController(title: "Modificacion", message: "El registro no pudo se modificado", preferredStyle: .alert)
                            alertC.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        }
                        
                        self.present(alertC, animated: true)
                    })
                }
                
            } else {
                alert = UIAlertController(title: "Error ", message: "No cumple con las reglas", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            }
            
            
            
        } else {
            alert = UIAlertController(title: "Password invalido", message: "Su password no es el correcto", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Correjir", style: .default, handler: { action in
                
                //self.changeView()
                print("")
            }))
            
            self.present(alert, animated: true)
        }
    }
    
    func toList() {
        performSegue(withIdentifier: "returnToList", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
