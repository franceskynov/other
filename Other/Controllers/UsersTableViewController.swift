//
//  UsersTableViewController.swift
//  Other
//
//  Created by Dev on 10/16/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit
import AlamofireImage

class UsersTableViewController: UITableViewController {

    var users = [User]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUsersData()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    
        navigationItem.leftBarButtonItem = editButtonItem
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return users.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "userCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? UserTableViewCell  else {
            fatalError("cannot load the cell")
        }
        let user = users[indexPath.row]
        
        cell.usernameLabel.text = user.username
        cell.emailLabel.text = user.email
        cell.CountryLabel.text = user.country
        
        return cell
    }
 

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            UserService.deleteUser(id: users[indexPath.row].id!, completation: { (success) -> Void in
                if success! {
                    
                    self.users.remove(at: indexPath.row )
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    print("deleted")
                    
                } else {
                    print("error")
                }
            })
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

        //super.prepare(for: segue, sender: sender)
        
        print(segue.identifier ?? "")
        
        switch (segue.identifier ?? "") {
            case "peopleDetails" :
                
                guard let usersDetailController = segue.destination as? UserDetailViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
            
                guard let selectedCell = sender as? UserTableViewCell else {
                    fatalError("Unexpected sender: \(String(describing: sender))")
                }
            
                guard let indexPath = tableView.indexPath(for: selectedCell) else {
                    fatalError("The selected cell is not being displayed by the table")
                }
            
                let selectedUser = users[indexPath.row]
                usersDetailController.user = selectedUser
            
            default:
                fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
        
    }
    
    @objc func refresh(_ sender: Any) {
        UserService.getUsers { (users) in
            self.users = []
            self.users = users
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
    
    /**
     *
     */
    func getUsersData() {
        UserService.getUsers { (users) in
            self.users = users
            //self.tableView.reloadData()
        }
        
        print("run")
    }

}
