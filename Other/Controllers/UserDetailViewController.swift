//
//  UserDetailViewController.swift
//  Other
//
//  Created by Dev on 10/19/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    var user: User?
    var tmp: User?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user =  user {
            usernameLabel.text = user.username
            emailLabel.text = user.email
            countryLabel.text = user.country
            self.tmp = user
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print(segue.identifier ?? "")
        
        switch (segue.identifier ?? "") {
        case "editUser" :
            
            guard let editUserViewController = segue.destination as? EditUserViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            editUserViewController.user = user
            
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
            
    }
    
    //@IBAction func editUser(_ sender: UIBarButtonItem) {
        //performSegue(withIdentifier: "EditUser", sender: self)
        ///let ed = EditUserViewController(nibName: "EditUserViewController", bundle: nil)
        
        ///navigationController?.pushViewController(ed, animated: true)
        //ed.user = tmp
        //print(tmp?.country ?? "")
   // }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
